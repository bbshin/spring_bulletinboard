<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>投稿画面</title>
</head>
<body>
	<ul>
		<li><a href="home">ホームへ</a></li>
		<li><a href="post">新規投稿</a></li>
		<li><a href="admin">管理者画面</a></li>
		<li><a href="logout">ログアウト</a></li>
	</ul>
	<h2>投稿画面</h2>
	<form:form modelAttribute="postForm" action="post">
		<%-- 		<input type="hidden" id="userId" name="userId" value="${loginUser.id}"/> <!-- valueが入らん --> --%>
		<form:input path="category" />
		<br>
		<form:input path="subject" />
		<br>
		<form:textarea path="text"></form:textarea>
		<br>
		<input type="submit" value="投稿する">
	</form:form>
</body>
</html>