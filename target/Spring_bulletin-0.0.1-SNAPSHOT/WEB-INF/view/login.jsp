<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
</head>
<body>
	<form:form modelAttribute="loginForm" action="login">
		<form:input path="loginId" /><br>
		<form:input path="password" /><br>
		<input type="submit" value="ログイン">
	</form:form>
</body>
</html>