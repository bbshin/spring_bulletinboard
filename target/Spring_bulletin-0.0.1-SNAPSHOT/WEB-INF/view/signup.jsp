<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>新規登録画面</title>
</head>
<body>
	<ul>
		<li><a href="home">ホームへ</a></li>
		<li><a href="post">新規投稿</a></li>
		<li><a href="admin">管理者画面</a></li>
		<li><a href="logout">ログアウト</a></li>
	</ul>
	<form:form modelAttribute="userForm" action="signup">
		ログインID<form:input path="loginId" /><br>
		名称<form:input path="name" /><br>
		パスワード<form:input path="password" /><br>
		支店<form:select path="branchId">
				<form:options items="${selectBranches}" itemLabel="name" itemValue="id" />
			</form:select>
		部署・役職<form:select path="divisionId">
					<form:options items="${selectDivisions}" itemLabel="name" itemValue="id" />
				</form:select>
		<input type="submit" value="登録">
	</form:form>
</body>
</html>