<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>管理者画面</title>
</head>
<body>
	<ul>
		<li><a href="home">ホームへ</a></li>
		<li><a href="post">新規投稿</a></li>
		<li><a href="signup">新規ユーザー登録</a></li>
		<li><a href="admin">管理者画面</a></li>
		<li><a href="logout">ログアウト</a></li>
	</ul>
</body>
</html>