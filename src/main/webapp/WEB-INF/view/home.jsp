<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<p>ログイン成功</p>
	<nav>
		<ul>
			<li><a href="home">ホームへ</a></li>
			<li><a href="post">新規投稿</a></li>
			<li><a href="admin">管理者画面</a></li>
			<li><a href="logout">ログアウト</a></li>
		</ul>
	</nav>
	<p>${loginUser.name}さん</p>
	<p>投稿一覧</p>
	<c:forEach items="${postList}" var="post">
		<p>${post.text}</p>
		<c:if test="${loginUser.id == post.userId }">
			<form:form modelAttribute= "postForm" action="deletePost">
				<form:hidden path="id" value="${post.id}" />
				<input type="submit" value="投稿削除">
			</form:form>
		</c:if>
		<c:forEach items="${commentList}" var="comment">
			<c:if test="${post.id == comment.postId}">
				== comment : <c:out value="${comment.text}" /> ==
				<c:if test="${loginUser.id == comment.userId}">
					<form:form modelAttribute="commentForm" action="deleteComment">
						<form:hidden path="id" value="${comment.id}" />
						<input type="submit" value="コメント削除">
					</form:form>
				</c:if><br>
			</c:if>
		</c:forEach>
		<form:form modelAttribute="commentForm" action="comment">
			<form:errors path="*" />
			<form:hidden path="postId" value="${post.id}" />
			<form:input path="text" />
			<input type="submit" value="コメントする">
		</form:form>
	</c:forEach>
</body>
</html>