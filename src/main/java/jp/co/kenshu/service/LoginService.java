package jp.co.kenshu.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.kenshu.dto.LoginUserDto;
import jp.co.kenshu.entity.LoginUserEntity;
import jp.co.kenshu.form.LoginForm;
import jp.co.kenshu.mapper.LoginMapper;

@Service
public class LoginService {

	@Autowired
	private LoginMapper loginMapper;

	public LoginUserDto login(LoginForm form) {
		LoginUserDto dto = new LoginUserDto();
		LoginUserEntity entity = loginMapper.login(form.getLoginId(), form.getPassword());
		if(entity == null) {
			dto = null;
		}else {
			BeanUtils.copyProperties(entity,  dto);
		}
		return dto;
	}
}
