package jp.co.kenshu.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.kenshu.dto.CommentDto;
import jp.co.kenshu.entity.CommentEntity;
import jp.co.kenshu.form.CommentForm;
import jp.co.kenshu.mapper.CommentMapper;

@Service
public class CommentService {

	@Autowired
	private CommentMapper commentMapper;

	public void comment(CommentForm form) {
		commentMapper.commentInsert(
				form.getUserId(), form.getPostId(), form.getText());
	}

	public List<CommentDto> getComment(){
		List<CommentEntity> ce = commentMapper.getCommentList();
		List<CommentDto> cd = convertToDto(ce);
		return cd;
	}

	private List<CommentDto> convertToDto(List<CommentEntity> ce){
		List<CommentDto> cd = new LinkedList<CommentDto>();
		for(CommentEntity entity : ce) {
			CommentDto dto = new CommentDto();
			BeanUtils.copyProperties(entity, dto);
			cd.add(dto);
		}
		return cd;
	}

	public void deleteComment(CommentForm form) {
		commentMapper.deleteComment(form.getId());
	}
}