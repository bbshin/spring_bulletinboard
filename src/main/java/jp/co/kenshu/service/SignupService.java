package jp.co.kenshu.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.kenshu.form.UserForm;
import jp.co.kenshu.mapper.SignupMapper;

@Service
public class SignupService {
	@Autowired
	private SignupMapper signupMapper;

	public void signupUser(UserForm form) {
		signupMapper.userInsert(
				form.getLoginId(), form.getName(), form.getPassword(), form.getBranchId(), form.getDivisionId());
	}
}
