package jp.co.kenshu.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.kenshu.dto.PostDto;
import jp.co.kenshu.entity.PostEntity;
import jp.co.kenshu.form.PostForm;
import jp.co.kenshu.mapper.PostMapper;

@Service
public class PostService {
	@Autowired
	private PostMapper postMapper;

	public void post(PostForm form) {
		postMapper.postInsert(
				form.getUserId(),form.getSubject(),form.getText(),form.getCategory());
	}

	public List<PostDto> getPost() {
		List<PostEntity> pe = postMapper.getPostList();
		List<PostDto> pd = convertToDto(pe);
		return pd;
	}

	private List<PostDto> convertToDto(List<PostEntity> pe){
		List<PostDto> pd = new LinkedList<PostDto>();
		for(PostEntity entity : pe) {
			PostDto dto = new PostDto();
			BeanUtils.copyProperties(entity, dto);
			pd.add(dto);
		}
		return pd;
	}

	public void deletePost(PostForm form) {
		postMapper.deletePost(form.getId());
	}
}
