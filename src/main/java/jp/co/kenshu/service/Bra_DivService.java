package jp.co.kenshu.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.kenshu.dto.BranchDto;
import jp.co.kenshu.dto.DivisionDto;
import jp.co.kenshu.entity.BranchEntity;
import jp.co.kenshu.entity.DivisionEntity;
import jp.co.kenshu.mapper.Bra_DivMapper;

@Service
public class Bra_DivService {

	@Autowired
	private Bra_DivMapper bdMapper;

	public List<BranchDto> getBranches() {
		List<BranchEntity> be = bdMapper.getBranchList();
		List<BranchDto> bd = branchConvertToDto(be);
		return bd;
	}

	public List<DivisionDto> getDivisions(){
		List<DivisionEntity> de = bdMapper.getDivisionList();
		List<DivisionDto> dd = divisionConvertToDto(de);
		return dd;
	}

	private List<BranchDto> branchConvertToDto(List<BranchEntity> be){
		List<BranchDto> bd = new LinkedList<BranchDto>();
		for(BranchEntity entity : be) {
			BranchDto dto = new BranchDto();
			BeanUtils.copyProperties(entity, dto);
			bd.add(dto);
		}
		return bd;
	}

	private List<DivisionDto> divisionConvertToDto(List<DivisionEntity> de){
		List<DivisionDto> dd = new LinkedList<DivisionDto>();
		for(DivisionEntity entity : de) {
			DivisionDto dto = new DivisionDto();
			BeanUtils.copyProperties(entity, dto);
			dd.add(dto);
		}
		return dd;
	}

}
