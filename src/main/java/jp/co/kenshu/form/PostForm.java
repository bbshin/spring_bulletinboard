package jp.co.kenshu.form;

import org.hibernate.validator.constraints.NotEmpty;

public class PostForm {
	private int id;
	private int userId;
	@NotEmpty
	public String category;
	@NotEmpty
	public String subject;
	@NotEmpty
	public String text;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
