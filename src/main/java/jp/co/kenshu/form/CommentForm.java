package jp.co.kenshu.form;

import org.hibernate.validator.constraints.NotEmpty;

public class CommentForm {
	private int id;
	private int postId;
	private int userId;
	@NotEmpty(message = "コメントを入力してください")
	private String text;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPostId() {
		return postId;
	}

	public void setPostId(int postId) {
		this.postId = postId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}