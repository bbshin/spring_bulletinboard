package jp.co.kenshu;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.kenshu.dto.BranchDto;
import jp.co.kenshu.dto.DivisionDto;
import jp.co.kenshu.form.UserForm;
import jp.co.kenshu.service.Bra_DivService;
import jp.co.kenshu.service.SignupService;

@Controller
public class SignupController {
	@RequestMapping(value="signup", method=RequestMethod.GET)
	public String goSignup(Model model) {

		model.addAttribute("userForm", new UserForm());
		model.addAttribute("selectBranches", getSelectBranches());
		model.addAttribute("selectDivisions", getSelectDivisions());
		return "signup";
	}

	@Autowired
	private SignupService signupService;

	@RequestMapping(value="signup", method=RequestMethod.POST)
	public String signup(@ModelAttribute UserForm form, Model model) {
		signupService.signupUser(form);
		return "redirect:/admin";
	}

	@Autowired
	private Bra_DivService bdService;

	private List<BranchDto> getSelectBranches(){
		List<BranchDto> list = bdService.getBranches();
		return list;
	}

	private List<DivisionDto> getSelectDivisions(){
		List<DivisionDto> list = bdService.getDivisions();
		return list;
	}
}
