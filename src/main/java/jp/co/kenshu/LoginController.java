package jp.co.kenshu;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.kenshu.dto.LoginUserDto;
import jp.co.kenshu.form.LoginForm;
import jp.co.kenshu.service.LoginService;

@Controller
public class LoginController {
	@RequestMapping(value="login", method=RequestMethod.GET)
	public String loginPage(Model model) {
		model.addAttribute("loginForm",new LoginForm());
		return "login";
	}

	@Autowired
	private LoginService loginService;

	@Autowired
	private HttpSession session;

	@RequestMapping(value="login", method=RequestMethod.POST)
	public String login(@ModelAttribute LoginForm form, Model model) {
		LoginUserDto userDto = loginService.login(form);
		if(userDto==null) {
			return "login";
		}
		session.setAttribute("loginUser", userDto);
	return "redirect:/home";
	}
}
