package jp.co.kenshu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.kenshu.form.CommentForm;
import jp.co.kenshu.service.CommentService;

@Controller
public class DeleteCommentController {
	@Autowired
	private CommentService commentService;

	@RequestMapping(value="deleteComment", method=RequestMethod.POST)
	public String deleteComment(@ModelAttribute CommentForm form, Model model) {
		commentService.deleteComment(form);
		return "redirect:/home";
	}
}
