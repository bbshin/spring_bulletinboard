package jp.co.kenshu;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.kenshu.dto.LoginUserDto;
import jp.co.kenshu.form.CommentForm;
import jp.co.kenshu.service.CommentService;

@Controller
public class CommentController {

	@Autowired
	private CommentService commentService;

	@Autowired
	private HttpSession session;


	@RequestMapping(value="comment", method=RequestMethod.POST)
	public String com(@Valid @ModelAttribute CommentForm form, BindingResult result, Model model) {
		if(result.hasErrors()) {
			System.out.println("エラー文");
		}else {
			LoginUserDto loginUserDto = (LoginUserDto) session.getAttribute("loginUser");
			form.setUserId(loginUserDto.getId());
			commentService.comment(form);
		}
		return "redirect:/home";
	}
}