package jp.co.kenshu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.kenshu.form.PostForm;
import jp.co.kenshu.service.PostService;

@Controller
public class DeletePostController {
	@Autowired
	private PostService postService;

	@RequestMapping(value="deletePost", method=RequestMethod.POST)
	public String deletePost(@ModelAttribute PostForm form, Model model) {
		postService.deletePost(form);
		return "redirect:/home";
	}
}
