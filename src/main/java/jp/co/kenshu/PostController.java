package jp.co.kenshu;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.kenshu.dto.LoginUserDto;
import jp.co.kenshu.form.PostForm;
import jp.co.kenshu.service.PostService;

@Controller
public class PostController {
	@RequestMapping(value="post", method=RequestMethod.GET)
	public String postPage(Model model) {
		model.addAttribute("postForm", new PostForm());
		return "post";
	}

	@Autowired
	private PostService postService;

	@Autowired
	HttpSession session;

	@RequestMapping(value="post", method=RequestMethod.POST)
	public String post(@Valid @ModelAttribute PostForm form, BindingResult result, Model model) {
		if (result.hasErrors()) {
	        model.addAttribute("message", "以下のエラーを解消してください");
	        return "post";
	    } else {
			LoginUserDto loginUserDto = (LoginUserDto) session.getAttribute("loginUser");
			form.setUserId(loginUserDto.getId());
			postService.post(form);
		}
		return "redirect:/home";
	}
}
