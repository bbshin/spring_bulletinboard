package jp.co.kenshu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.kenshu.form.CommentForm;
import jp.co.kenshu.form.PostForm;
import jp.co.kenshu.service.CommentService;
import jp.co.kenshu.service.PostService;

@Controller
public class HomeController {

	@Autowired
	private PostService postService;

	@Autowired
	private CommentService commentService;

	@RequestMapping(value="home", method=RequestMethod.GET)
	public String goHome(Model model) {
		model.addAttribute("postList", postService.getPost());
		model.addAttribute("commentList", commentService.getComment());
		model.addAttribute("postForm", new PostForm());
		model.addAttribute("commentForm", new CommentForm());
		return "home";
	}
}
