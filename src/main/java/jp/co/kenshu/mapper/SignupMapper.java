package jp.co.kenshu.mapper;

public interface SignupMapper {
	void userInsert(String loginId, String name, String pass, int branchId, int divisionId);
}
