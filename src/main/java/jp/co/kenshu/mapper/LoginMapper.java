package jp.co.kenshu.mapper;

import jp.co.kenshu.entity.LoginUserEntity;

public interface LoginMapper {
	LoginUserEntity login(String loginId, String pass);
}
