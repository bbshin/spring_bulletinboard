package jp.co.kenshu.mapper;

import java.util.List;

import jp.co.kenshu.entity.BranchEntity;
import jp.co.kenshu.entity.DivisionEntity;

public interface Bra_DivMapper {
	List<BranchEntity> getBranchList();
	List<DivisionEntity> getDivisionList();
}
