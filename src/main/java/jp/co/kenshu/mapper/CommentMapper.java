package jp.co.kenshu.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import jp.co.kenshu.entity.CommentEntity;

public interface CommentMapper {
	void commentInsert(@Param("userId") int userId,
						@Param("postId") int postId,
						@Param("text") String text);

	List<CommentEntity> getCommentList();
	void deleteComment(int commentId);
}