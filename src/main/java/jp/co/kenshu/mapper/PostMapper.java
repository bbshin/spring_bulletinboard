package jp.co.kenshu.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import jp.co.kenshu.entity.PostEntity;

public interface PostMapper {
	void postInsert(@Param("userId") int userId,
					@Param("category") String category,
					@Param("subject") String subject,
					@Param("text") String text);
	List<PostEntity> getPostList();
	void deletePost(int postId);
}